### What is this repository for? ###

A tiny Django app I slapped together after things went wrong during a coding challenge. XD

It pulls weather from http://www.wunderground.com/ for Cincinnati and shows it.

### How do I get set up? ###

Do you really want to use try this for something? Why?

If you're sure, then all you really need is to have Python 3.4+ and Django 1.6+ installed. All other dependencies are included.

The url string may need to be replaced if with a new one from Weather Underground. Just sign up for one on their site if you need it.

### Contribution guidelines ###

Don't contribute to this.