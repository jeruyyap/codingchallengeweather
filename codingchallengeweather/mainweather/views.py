from django.shortcuts import render
import urllib
import json

# Create your views here.
def index(request):
    f = urllib.request.urlopen('http://api.wunderground.com/api/ab3d94cc2564b618/geolookup/conditions/q/IA/Cincinnati.json')
    str_response = f.readall().decode('utf-8')
    parsed_json = json.loads(str_response)
    location = parsed_json['location']['city']
    temp_c = parsed_json['current_observation']['temperature_string']
    humidity = parsed_json['current_observation']['relative_humidity']
    wind = parsed_json['current_observation']['wind_string']
    timeupdated = parsed_json['current_observation']['observation_time']
    print("Current temperature in %s is: %s" % (location, temp_c))
    f.close()
    context = {'mylocation': location, 'mytemperature': temp_c, 'myhumidity': humidity, 'mywind': wind,
               'mytimeupdated': timeupdated}
    return render(request, 'mainweather/index.html', context)